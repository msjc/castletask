/******************
tcp_server.h
Author: Matthew Castellana
Adapted from Boost Examples
Purpose: The Boost TCP server
which sends out the TaskList. 
*******************/

#pragma once
#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <ctime>
#include <iostream>
#include <string>

#include <gtkmm/window.h>


#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

#include "tcp_connection.h"
#include "main_screen.h"

using boost::asio::ip::tcp;

// forward declarations
class MainScreen;
class tcp_connection;

typedef boost::shared_ptr<tcp_connection> pointer;

class tcp_server
{
	public:
	  
	  tcp_server(boost::asio::io_service& io_service, MainScreen & new_main_window);

	private:
	  void start_accept();

	  void handle_accept(pointer new_connection,
	      const boost::system::error_code& error);

	  tcp::acceptor acceptor_;
	  MainScreen & main_window;
};

#endif