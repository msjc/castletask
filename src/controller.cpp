/******************
controller.cpp
Author: Matthew Castellana
*******************/

#include <cstdio>

#include "controller.h"

Controller::Controller(Glib::RefPtr<Gtk::Application> new_app){
	// Set the app
	app = new_app;

	// Create the main window
	main_screen = new MainScreen();
	main_screen->set_controller(this); 

	// Create the task list data storage
  	std::string task_list_name = "My List";
  	task_list = new TaskList(task_list_name);

  	this->load_xml();
  	this->refresh();
}

Controller::~Controller(){
	// Convert the tasks to XML
  	this->convert_tasks_to_xml(task_list);

  	// Save to disk.
  	XMLError error_check = tasks.SaveFile(save_file_location.c_str());
  	if (error_check != 0){
    	std::cout << "Error in saving tasks file." << std::endl;
  	}
}

int Controller::run(){
	return app->run(*main_screen);
}


// Adds a Task to the TaskList
void Controller::add_task(Task * new_task){
  task_list->add_task(new_task);
  this->refresh();
}

void Controller::edit_task(Task * task_to_edit, Task * edited_task){
  task_list->edit_task(task_to_edit, edited_task);
  this->refresh();
}

// Removes a specified Task from the TaskList
void Controller::remove_task(std::string name, std::string date_string){
  // Find and remove Task in data storage
  task_list->remove_task(name, date_string);

  // Refresh rows
  this->refresh();

}

Task * Controller::fetch_task(std::string name, std::string date_string){
  try {
    return task_list->fetch_task(name, date_string);
  }
  catch (int i) {
    std::cerr << "Controller Error: Task to edit not found in task list." << std::endl;
    throw -1;
  }
}


// Refreshes the GUI elements representing the Tasks
void Controller::refresh(){

	// Clear the task list rows
	main_screen->clear_task_list();

	// Re-add all the rows.
	// Create task rows based on TaskList and append them.
	for (int i = 0; i < task_list->getLength(); ++i){
		Task * temp = task_list->getTaskAtPosition(i);
		main_screen->push_back_task_row(temp->get_name(), temp->get_date_as_string(true));
	}
}

// Loads an XML from the user's hard drive.
// Default directory is ~/Documents/CastleTask/
// Default file name is tasks.xml.
void Controller::load_xml(){
    // Load the XML to-do list from memory
  passwd *pw = getpwuid(getuid());
  const char *home = pw->pw_dir;

  // Check for the directory, and create it if it's not there (in Documents/TaskList)
  struct stat stat_info;
  save_dir = std::string(home);
  save_dir = save_dir + "/Documents/CastleTask";

  if(stat(save_dir.c_str(),&stat_info) != 0) { // if the directory is not present 
    // Create the directory.
    mkdir(save_dir.c_str(), S_IRWXU); // user RWX
  }

  // Check if the tasks.xml file is there

  save_file_location = save_dir + "/tasks.xml";
  if (stat(save_file_location.c_str(), &stat_info) == 0){ // if the file exists
    // load it
    XMLError error = tasks.LoadFile(save_file_location.c_str());

    // Check for errors
    if (error != 0){
      std::cout << "Error in loading file: " << error << std::endl;
      exit(error);
    }

    // No errors, populate the file from save
    XMLNode * root = tasks.FirstChild();
    if (root == nullptr) {
      std::cerr << "No XML root found." << std::endl;
      exit(XML_ERROR_FILE_READ_ERROR);
    }

    XMLElement * task = root->FirstChildElement("Task");

    //@todo: code breaks if empty string task is loaded
    if (task != nullptr) { // there is at least one task
      this->load_task_from_xml(task);
      task = task->NextSiblingElement("Task");
      while (task != NULL){
        this->load_task_from_xml(task);
        task = task->NextSiblingElement("Task");      
      }
    }
  }
}

// Loads an individual task from the XML parser
void Controller::load_task_from_xml(XMLElement * task){
  // Create a task using the name element only
  XMLElement * task_data = task->FirstChildElement("Name");
  if (task_data == nullptr) {
    std::cerr << "No Name element found." << std::endl;
    exit(XML_ERROR_FILE_READ_ERROR);
  }  
  Task * new_task = new Task(new std::string(task_data->GetText()));

  // Load the date info into the task
  task_data = task->FirstChildElement("Date");
  if (task_data == nullptr) {         // Right now date is required
    std::cerr << "No Date element found." << std::endl;
    exit(XML_ERROR_FILE_READ_ERROR);
  }  
  int month, day, year, hour, minute;
  task_data->QueryIntAttribute("Month", &month);
  task_data->QueryIntAttribute("DayOfMonth", &day);
  task_data->QueryIntAttribute("Year", &year);
  task_data->QueryIntAttribute("Hour", &hour);
  task_data->QueryIntAttribute("Minute", &minute);
  new_task->set_date_time(month, day, year, hour, minute);
  new_task->create_chrono_point_from_date_time(); // set the chrono point from the date_time tm struct,
                                                  // since it is also used

  // The task is finished, add it.
  task_list->add_task(new_task);
}

// Convert the TaskList to XML format for saving
void Controller::convert_tasks_to_xml(TaskList * tasklist){
  // Clear the TinyXML data structure we created at load
  tasks.Clear();

  // Insert a root node
  XMLNode * root = tasks.NewElement("Root");
  tasks.InsertFirstChild(root);

  // Insert tasks
  for (int i = 0; i < tasklist->getLength(); ++i){
    XMLElement * task = tasks.NewElement("Task");

    XMLElement * task_name = tasks.NewElement("Name");
    task_name->SetText((tasklist->getTaskAtPosition(i))->get_name().c_str());
    task->InsertEndChild(task_name);

    XMLElement * task_date = tasks.NewElement("Date");
    task->InsertEndChild(task_date);

    task_date->SetAttribute("Month", (tasklist->getTaskAtPosition(i))->get_month());
    task_date->SetAttribute("DayOfMonth", (tasklist->getTaskAtPosition(i))->get_day_of_month());
    task_date->SetAttribute("Year", (tasklist->getTaskAtPosition(i))->get_year());
    task_date->SetAttribute("Hour", (tasklist->getTaskAtPosition(i))->get_hour());
    task_date->SetAttribute("Minute", (tasklist->getTaskAtPosition(i))->get_minute());

    root->InsertEndChild(task);
  }
}

// Used by SyncWindow to access the TaskList for syncing
TaskList* Controller::get_list_for_sync(){
  return task_list;
}

// Combines the elements of two TaskLists by comparing them individually.
// Note that the current implementation does not handle advanced situations:
// (e.g., three devices in sync. Device 1 deletes a task and syncs with
// Device 2, which also deletes the task. Device 2 syncs with Device 3. In the ideal
// case, the task should be deleted from Device 3. In the current implementation, Device 2
// will re-add the task because it doesn't see it in its current list of tasks.)
// @todo: Better algorithm for comparison.
void Controller::sync_task_lists(TaskList* new_task_list){
  std::vector<Task*> tasks_to_add; // tasks not found in current list
  for (int i = 0; i < new_task_list->getLength(); ++i){ // for each task in the new list
    bool already_in = false; // assume it's not in
    for (int j = 0; j < task_list->getLength(); ++j){ // compare to each task in the old list
      already_in = already_in || (new_task_list->getTaskAtPosition(i)->compare(task_list->getTaskAtPosition(j)));
    }

    if (!already_in){ // if it's not in, add it
      tasks_to_add.push_back(new_task_list->getTaskAtPosition(i));
    }
  }
  // now add the confirmed new tasks to the original list
  for (int i = 0; i < tasks_to_add.size(); ++i){
    task_list->add_task(tasks_to_add[i]);
  }
  refresh();
}