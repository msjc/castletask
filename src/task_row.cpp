/******************
task_row.cpp
Author: Matthew Castellana
*******************/

#include <glibmm/ustring.h>
#include "task_row.h"

TaskRow::TaskRow(){
}

TaskRow::TaskRow(std::string name, std::string date_string, MainScreen * new_main_window) {
	// Add the layout container
	add(event_box);
	
	// Store the data
	main_window = new_main_window;

	// Set the labels
	task_text.set_text(Glib::ustring(name));
	date_text.set_text(Glib::ustring(date_string));
	date_text.set_padding(10, 0);

	// Pack widgets into container
	task_container.pack_start(checkbox, Gtk::PACK_SHRINK);
	task_container.pack_start(task_text, Gtk::PACK_SHRINK);
	task_container.pack_start(date_text, Gtk::PACK_SHRINK);
	event_box.add(task_container);

	// Set the Event Mask so that you can accept double click signal (button presses)
	this->set_events(Gdk::BUTTON_PRESS_MASK);
	this->add_events(Gdk::BUTTON_PRESS_MASK);
	event_box.set_events(Gdk::BUTTON_PRESS_MASK);
	event_box.add_events(Gdk::BUTTON_PRESS_MASK);

	event_box.show_all_children();

	event_box.show();
	task_container.show();
	task_container.show_all_children();

	// Attach signals
	checkbox.signal_clicked().connect(sigc::mem_fun(*this,
              &TaskRow::on_checkbox_clicked));
	
	event_box.signal_button_press_event().connect(sigc::mem_fun(*this, &TaskRow::double_click_signal_handler_event), false);


}

TaskRow::~TaskRow(){

}

// Remove task when checkbox is clicked
void TaskRow::on_checkbox_clicked(){
	main_window->remove_task(task_text.get_text(), date_text.get_text());
}

void TaskRow::on_edit_button_clicked(){
	main_window->edit_task_button_clicked(task_text.get_text(), date_text.get_text());
}


int TaskRow::double_click_signal_handler_event(GdkEventButton *event)
{
  	if (event->type==GDK_2BUTTON_PRESS) { // edit task on double click
  		main_window->edit_task_button_clicked(task_text.get_text(), date_text.get_text());
  	}

  	return false;
}
