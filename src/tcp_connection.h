/******************
tcp_connection.h
Author: Matthew Castellana
Adapted from Boost Examples
Purpose: The Boost TCP connection class
which handles the actual connection. 
*******************/

#pragma once
#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include <ctime>
#include <iostream>
#include <string>
#include <sstream>

#include <gtkmm/window.h>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "main_screen.h"
#include "task_list.h"

using boost::asio::ip::tcp;

// forward declarations
class MainScreen;

class tcp_connection
  : public boost::enable_shared_from_this<tcp_connection>
{
  public:
    typedef boost::shared_ptr<tcp_connection> pointer;

    static pointer create(boost::asio::io_service& io_service, MainScreen & new_main_window)
    {
      return tcp_connection::pointer(new tcp_connection(io_service, new_main_window));
    }
    tcp_connection(boost::asio::io_service& io_service, MainScreen & new_main_window);
    tcp::socket& socket();

    void start();

  private:
    void handle_write(const boost::system::error_code& /*error*/,
        size_t /*bytes_transferred*/);

    tcp::socket socket_;
    MainScreen & main_window;
};

#endif