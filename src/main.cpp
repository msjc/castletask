/******************
main.cpp
Author: Matthew Castellana
Purpose: The main file for the program.
Think of this as the "controller" in the
"model-view-controller" design pattern.

*******************/

#include "controller.h"


int main(int argc, char* argv[]){

	auto app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");

	app->set_flags(Gio::APPLICATION_NON_UNIQUE); // this allows multiple instance to run
												 // on the same computer

	Controller new_controller(app);
	
	return new_controller.run();
}
