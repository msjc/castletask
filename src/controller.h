/******************
controller.h
Author: Matthew Castellana
Purpose: Serves as the Controller in the
Model-View-Controller design pattern for the
application.
*******************/


#pragma once
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <gtkmm/application.h>

#include "tinyxml2.h"
#include "main_screen.h"
#include "task_list.h"

using namespace tinyxml2;

// forward declarations
class MainScreen;

class Controller
{
	public:
		Controller(Glib::RefPtr<Gtk::Application> new_app);
		virtual ~Controller();

  		int run();

  		// Add/Remove Tasks
  		void add_task(Task * new_task);
  		void edit_task(Task * task_to_edit, Task * edited_task);
  		void remove_task(std::string name, std::string date_string);

  		// Fetch Tasks
  		Task * fetch_task(std::string name, std::string date_string);

  		// Update the task rows (GUI elements) to reflect the updated data
  		void refresh(); 

  		TaskList* get_list_for_sync();
  		void sync_task_lists(TaskList* new_task_list);

	private:
		Glib::RefPtr<Gtk::Application> app;
		MainScreen * main_screen;
		

		// Data
		TaskList * task_list; // stores the actual Task data elements
		std::string save_dir; // location of user's save folder
		std::string save_file_location; // path to save file
		XMLDocument tasks; // used for saving to xml with TinyXML2

		// XML Saving/Loading Tasks
		void load_xml(); // load all tasks
		void load_task_from_xml(XMLElement * task); // specifics of loading one task
		void convert_tasks_to_xml(TaskList * tasklist); // saving tasks
};

#endif
