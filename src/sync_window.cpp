/******************
sync_window.cpp
Author: Matthew Castellana 
*******************/

#include "sync_window.h"

SyncWindow::SyncWindow()
{
}

SyncWindow::SyncWindow(MainScreen * new_main_window, int new_type)
: start("Start"), sync("Sync")
{
	this->set_modal(true);
	type = new_type;

	if (type == 1) { // server
		this->set_title("Sync as Server");
	}
	else { // client
		this->set_title("Sync as Client");
	}
	this->set_default_size(150, 100);

	main_window = new_main_window;

	Gtk::Box * dialog_vbox = get_vbox(); // get the layout

	// pack window differently depending on client or server
	dialog_vbox->pack_start(status);
	if (type == 1){
		dialog_vbox->pack_start(start);
		c = start.signal_clicked().connect(sigc::mem_fun(*this,
              &SyncWindow::server_start_button_clicked));
		status.set_text("Server");
	}
	else {
		dialog_vbox->pack_start(sync);
		c = sync.signal_clicked().connect(sigc::mem_fun(*this,
              &SyncWindow::client_sync_button_clicked));
		status.set_text("Client");
	}

	cancel = false;
}

SyncWindow::~SyncWindow(){	
}

// As Server, constantly look for connections unless cancel has been triggered
void look_for_connections(bool const & cancel, boost::asio::io_service & io_service, Gtk::Label & status, MainScreen & main_window){
	try
	{
		tcp_server server(io_service, main_window);
		while (cancel == false){
			io_service.run();
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}		
}

// Set up a thread for IO to prevent GUI lockup
void SyncWindow::look_for_connections_handler(){
	start.set_label("Cancel"); // change the button text
	c.disconnect(); // disconnect the "Start" callback
	c = start.signal_clicked().connect(sigc::mem_fun(*this, // attach the "Cancel" callback
              &SyncWindow::server_cancel_button_clicked));

	sync_thread = new std::thread(look_for_connections, std::ref(cancel), std::ref(io_service), std::ref(status), std::ref(*main_window));
}

// Start looking for a Server on the local network
// @todo: Code for two-way sync with one button click.
void SyncWindow::client_sync_button_clicked(){
	try
	{
		tcp::resolver resolver(io_service);
		tcp::resolver::query query("0.0.0.0", "8080");
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		tcp::socket socket(io_service);
		boost::asio::connect(socket, endpoint_iterator);

		for (;;)
		{
			boost::array<char, 128> buf;
			boost::system::error_code error;

			size_t len = socket.read_some(boost::asio::buffer(buf), error);

			if (error == boost::asio::error::eof)
				break; // Connection closed cleanly by peer.
			else if (error)
				throw boost::system::system_error(error); // Some other error.

			std::stringstream stream;
			stream << std::string(buf.data());
			boost::archive::text_iarchive unarchive(stream); // data is serialized
															 // un-serialize it
			TaskList temp;
			unarchive >> temp; // back into TaskList form

			// set status label
			status.set_text(temp.getName());

			// sync the task lists
			main_window->sync_task_lists(&temp);
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

// Callback for Server's Start button
void SyncWindow::server_start_button_clicked()
{
	look_for_connections_handler();
}

// Callback for Server's Cancel button
void SyncWindow::server_cancel_button_clicked()
{
	SyncWindow::cancel = true; // set cancel to true
	io_service.stop(); // stop io_service
	sync_thread->join(); // wait for thread to finish
	io_service.reset(); // reset the io service

	// Reset the Cancel button to Start
	c.disconnect();
	start.set_label("Start");
	c = start.signal_clicked().connect(sigc::mem_fun(*this,
              &SyncWindow::server_start_button_clicked));
	cancel = false;
}