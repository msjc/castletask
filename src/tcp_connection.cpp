/******************
tcp_connection.cpp
Author: Matthew Castellana
Adapted from Boost Examples
*******************/

#include "tcp_connection.h"

static pointer create(boost::asio::io_service& io_service, MainScreen & new_main_window)
  {
    return pointer(new tcp_connection(io_service, new_main_window));
  }

tcp::socket& tcp_connection::socket()
{
	return socket_;
}

void tcp_connection::start()
{
	// serialize the data
	std::ostringstream stream;
	boost::archive::text_oarchive archive(stream); 
	TaskList temp = *(main_window.get_list_for_sync());
	archive << temp;
	std::string to_send = stream.str();

	boost::asio::async_write(socket_, boost::asio::buffer(to_send),
	    boost::bind(&tcp_connection::handle_write, shared_from_this(),
	      boost::asio::placeholders::error,
	      boost::asio::placeholders::bytes_transferred));\
}


tcp_connection::tcp_connection(boost::asio::io_service& io_service, MainScreen & new_main_window)
: socket_(io_service), main_window(new_main_window)
{
}

void tcp_connection::handle_write(const boost::system::error_code& /*error*/,
  size_t /*bytes_transferred*/)
{
}
