/******************
task_list.h
Author: Matthew Castellana
Purpose: Data structure class used 
for storing Tasks.
*******************/

#pragma once
#ifndef TASK_LIST_H
#define TASK_LIST_H

#include <string>
#include <vector>
#include <algorithm>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include "tinyxml2.h"
#include "task.h"

using namespace tinyxml2;

class TaskList {
	public:
		TaskList();
		TaskList(std::string task_list_name);
		~TaskList();

		// Getters
		int getLength();
		std::string getName();
		Task * getTaskAtPosition(int position);
		Task * fetch_task(std::string name, std::string date_string);
		int fetch_task_index(std::string name, std::string date_string);

		// Modifiers
		void create_task(std::string task_name, XMLElement * task_data);
		void add_task(Task * new_task);
		void edit_task(Task * task_to_edit, Task * edited_task);
		void remove_task(std::string name, std::string date_string);

		// Serialization
		template <typename Archive>
		void serialize(Archive &ar, const unsigned int version) { 
			ar & name;
			ar & list; 
		}

		void print(); // print all tasks for debug

	private:
		friend class boost::serialization::access; // allows boost to access private variables
		std::string name;
		std::vector<Task *> list;

};
#endif