/******************
main_screen.cpp
Author: Matthew Castellana
*******************/

#include <iostream>

#include "main_screen.h"

// Constructor for the main window
MainScreen::MainScreen()
: add_task_button("+"), sync_as_server_button("Sync as Server"), sync_as_client_button("Sync as Client")
{
  // Set New Task Window to NULL - no new task window opened initially
  new_task_window = NULL;

  // Sets the border width of the window
  set_border_width(10);

  // Set window size and title
  set_default_size(1024, 768);
  set_title("CastleTask");

  // Create the task list data storage
  // std::string task_list_name = "My List";
  // task_list = new TaskList(task_list_name);

  //Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  // Button Handlers
  // When the button receives the "clicked" signal, it will call the
  // associated method.

  add_task_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MainScreen::add_task_button_clicked));

  sync_as_server_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MainScreen::sync_as_server_button_clicked));

  sync_as_client_button.signal_clicked().connect(sigc::mem_fun(*this,
              &MainScreen::sync_as_client_button_clicked));

  // Add the main layout box (top row for buttons, bottom row for tasks)
  add(main_layout);

  // Add buttons and other elements
  main_layout.pack_start(top_row, Gtk::PACK_SHRINK);
  main_layout.pack_start(m_ScrolledWindow);

  // Add the TreeView, inside a ScrolledWindow
  m_ScrolledWindow.add(m_ListBox);

  top_row.pack_start(add_task_button, false, false);
  top_row.pack_start(sync_as_server_button, false, false);
  top_row.pack_start(sync_as_client_button, false, false);

  // Show the elements
  m_ScrolledWindow.show_all();
  m_ScrolledWindow.show_all_children(true);
  top_row.show();
  add_task_button.show();
  sync_as_server_button.show();
  sync_as_client_button.show();
  main_layout.show();
}

// Destructor for the main window.
// Necessary for saving the current tasks
// to XML upon close.
MainScreen::~MainScreen()
{

}

void MainScreen::set_controller(Controller * new_controller){
  controller = new_controller;
}

void MainScreen::clear_task_list(){
    // Get all the rows
  std::vector<Widget *> rows = m_ListBox.get_children();

  // Remove all the rows
  for (int i = 0; i < task_row_array.size(); ++i){
    m_ListBox.remove(*task_row_array[i]);
  }

  // Clear the task_row_storage
  task_row_array.erase(task_row_array.begin(), task_row_array.end());
}

void MainScreen::push_back_task_row(std::string name, std::string date_string){
  task_row_array.push_back(new TaskRow(name, date_string, this));
  m_ListBox.append(*(task_row_array[task_row_array.size() - 1]));

    // Show the new children
  m_ListBox.show_all_children();
}

void MainScreen::add_task(Task * new_task){
  controller->add_task(new_task);
}

void MainScreen::edit_task(Task * task_to_edit, Task * edited_task){
  controller->edit_task(task_to_edit, edited_task);
}

void MainScreen::remove_task(std::string name, std::string date_string){
  controller->remove_task(name, date_string);
}


TaskList* MainScreen::get_list_for_sync(){
  return controller->get_list_for_sync();
}

void MainScreen::sync_task_lists(TaskList* new_task_list){
  controller->sync_task_lists(new_task_list);
}

// Callback function for "Add Task" button
void MainScreen::add_task_button_clicked()
{
  if (new_task_window != NULL) {
    delete new_task_window;
  }
  std::cout << "Before" << std::endl;
  new_task_window = new TaskCreationWindow(this);
  std::cout << "After" << std::endl;
  new_task_window->set_transient_for(*this);  // allows it to prevent use of the main window
                                              // while the dialog is open
  new_task_window->show_all_children(true); // show all the items in the window once created
  new_task_window->show(); // show the window itself
  new_task_window->initialize_focus(); // place the cursor in the name slot
}


void MainScreen::edit_task_button_clicked(std::string name, std::string date_string){
  // create Edit Task window populated with information from TaskRow

  Task * edited_task;

  // first, find fetch task from controller
  try {
    edited_task = controller->fetch_task(name, date_string);
  }
  catch (int i) {
    std::cerr << "Main Screen error" << i << ": Task not returned from Controller." << std::endl;
    return;
  }
  
  // then, create a TaskCreationWindow using the task
  if (new_task_window != NULL) {
    delete new_task_window;
  }
  new_task_window = new TaskCreationWindow(this, edited_task);
  new_task_window->set_transient_for(*this);  // allows it to prevent use of the main window
                                              // while the dialog is open
  new_task_window->show_all_children(true); // show all the items in the window once created
  new_task_window->show(); // show the window itself
  new_task_window->initialize_focus(); // place the cursor in the name slot
}

// Callback function for "Sync as Server" button
void MainScreen::sync_as_server_button_clicked()
{
  new_sync_window = new SyncWindow(this, 1); // 1 is for Server, 2 for Client
  new_sync_window->set_transient_for(*this);
  new_sync_window->show_all_children(true);
  new_sync_window->show();
}

// Callback function for "Sync as Client" button
void MainScreen::sync_as_client_button_clicked()
{
  new_sync_window = new SyncWindow(this, 2);
  new_sync_window->set_transient_for(*this);
  new_sync_window->show_all_children(true);
  new_sync_window->show();
}


// Closes the window
void MainScreen::closeWindow(){
}


