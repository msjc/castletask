/******************
task_creation_window.cpp
Author: Matthew Castellana
*******************/

#include "task_creation_window.h"

TaskCreationWindow::TaskCreationWindow() 
: ok_button("OK")
{

}

TaskCreationWindow::TaskCreationWindow(MainScreen * new_main_window) 
: ok_button("OK")
{
	this->initialize_window(new_main_window);
	edited_task = false;
}

TaskCreationWindow::TaskCreationWindow(MainScreen * new_main_window, Task * new_task_to_edit) 
: ok_button("OK")
{
	this->initialize_window(new_main_window);
	edited_task = true;
	task_to_edit = new_task_to_edit;
	
	// load data from task
	task_name_entry.set_text(task_to_edit->get_name());
	// calendar
	// time

}


TaskCreationWindow::~TaskCreationWindow() {
}


void TaskCreationWindow::initialize_focus(){
	// Place the cursor in the task name entry
	task_name_entry.grab_focus();
}

void TaskCreationWindow::ok_button_clicked()
{

	// Create and add a task.

	// Name
	std::string task_name = task_name_entry.get_text().raw();

	if (task_name == ""){
		this->close();
		return;
	}

	// Date
	unsigned int year;
	unsigned int month;
	unsigned int day;
	calendar.get_date(year, month, day);

	// Time
	unsigned int hour = std::stoi(hour_button.get_text());
	if (am_pm.get_active_text() == "P.M."){
		hour += 12;
	}
	else { // AM
		if (hour == 12){ // currently shows 12 in task window
			hour = 0; // for tm
		}

	}

	unsigned int minute = std::stoi(minute_button.get_text());

	// Change format from Gtk::Calendar to std::chrono input.
	// NOTE: while get_time requires months from 01-12, struct tm
	// stores them as 00-11!
	month += 1;
	if ((hour == 12) && (am_pm.get_active_text() == "A.M.")){ // midnight
		hour = 0;
	}	

	// Format the date/time
	std::stringstream time_data;
	time_data << year << " ";

	if (month < 10){
		time_data << "0"; // leading 0
	}
	time_data << month << " ";

	if (day < 10){
		time_data << "0"; // leading 0	 
	}
	time_data << day << " ";

	if (hour < 10){
		time_data << "0"; // leading 0
	}
	time_data << hour << " ";

	if (minute < 10){
		time_data << "0"; // leading 0
	}
	time_data << minute;

	// Get it into struct tm format
	struct tm * time = new struct tm;
	time_data >> std::get_time(time, "%Y %m %d %H %M");

	// Initialize other spaces
	std::cout << "Time data is " << time_data.str() << std::endl;
	std::cout << "Hour immediately is " << time->tm_hour << std::endl;
	time->tm_sec = 0; // we don't care about seconds
	time->tm_isdst = -1; // tell to check locale

	// Create the task and add/edit it
	Task * new_task = new Task(&task_name, time);

	std::cout << new_task->get_date_as_string(true) << std::endl;

	if (edited_task){
		main_window->edit_task(task_to_edit, new_task);
	}
	else {
		main_window->add_task(new_task);
	}
	
	std::cout << "Hour:" << hour << " Minute:" << minute << " Month: " << month << " Day:" << day << " Year:" << year << std::endl;
  	
	// Close the dialog
  	this->close();
}

void TaskCreationWindow::initialize_window(MainScreen * new_main_window) {
	// set up dialog
	this->set_modal(true);
	this->set_title("Add Task");

	main_window = new_main_window;

	Gtk::Box * dialog_vbox = get_vbox();

	  // Add Task Button Functionality
  	ok_button.signal_clicked().connect(sigc::mem_fun(*this,
              &TaskCreationWindow::ok_button_clicked));

  	// Task Name
	enter_text_label.set_text("Task Name: ");
	task_name_entry_row.pack_start(enter_text_label, Gtk::PACK_SHRINK);
	task_name_entry_row.pack_start(task_name_entry, Gtk::PACK_SHRINK);

	// Date/Time
	two_columns.pack_start(task_name_entry_row, Gtk::PACK_SHRINK);
	two_columns.pack_start(date_time_box, Gtk::PACK_SHRINK);
	date_time_box.pack_start(time_box, Gtk::PACK_SHRINK);
	date_time_box.pack_start(calendar, Gtk::PACK_SHRINK);

	// Set up the time
	hour_button.set_property("orientation", Gtk::ORIENTATION_VERTICAL);
	hour_button.set_range(1, 12);
	hour_button.set_wrap(true);
	hour_button.set_increments(1, 1);
	this->configure_time_divider();
	minute_button.set_property("orientation", Gtk::ORIENTATION_VERTICAL);
	minute_button.set_range(0, 59);
	minute_button.set_increments(1, 1);
	minute_button.set_wrap(true);

	am_pm.append(Glib::ustring("A.M."));
	am_pm.append(Glib::ustring("P.M."));

	this->set_time();

	// Allow for two-digit formatting of minute
	minute_button.signal_changed().connect( sigc::mem_fun(*this,
              &TaskCreationWindow::minute_digits_changed) );
	time_box.pack_start(hour_button, Gtk::PACK_SHRINK);
	time_box.pack_start(time_divider, Gtk::PACK_SHRINK);
	time_box.pack_start(minute_button, Gtk::PACK_SHRINK);
	time_box.pack_start(am_pm, Gtk::PACK_SHRINK);

	dialog_vbox->pack_start(two_columns, Gtk::PACK_SHRINK);
	dialog_vbox->pack_start(ok_button, Gtk::PACK_SHRINK);
}

// Change the size of the ":" between hour and minute
void TaskCreationWindow::configure_time_divider(){
	// Set the text
	time_divider.set_text(":");
	
	// Create a Pango attribute list containing the update font size
	Pango::AttrList * attrs = new Pango::AttrList();
	Pango::Attribute attr = Pango::Attribute::create_attr_size(30 * PANGO_SCALE);
	attrs->insert(attr); 

	// Set it
	time_divider.set_attributes(*attrs);

	delete attrs;
}

// initializes time to current time
void TaskCreationWindow::set_time(){
	// Get the time struct
	std::time_t current_time = time(NULL);
	struct tm *local_time = localtime(&current_time);

	// Set hour
	int hour = local_time->tm_hour;
	bool pm = false;
	if (hour > 12) {
		hour -= 12;
		pm = true;
	}
	if (hour == 0){
		hour = 12;
	}
	hour_button.set_text(Glib::ustring(std::to_string(hour)));

	// Set minutes
	std::ostringstream minutes;
	if (local_time->tm_min < 10){
		minutes << "0";
	}
	minutes << std::to_string(local_time->tm_min);
	minute_button.set_text(Glib::ustring(minutes.str()));
	
	// Set AM/PM
	if (pm){
		am_pm.set_active_text("P.M.");
	}
	else {
		am_pm.set_active_text("A.M.");
	}

}

// Formatting of minutes to text
void TaskCreationWindow::minute_digits_changed(){
	std::ostringstream str;
	double value = minute_button.get_value();
	if (value < 10){
		str << "0";
	}
  	str << minute_button.get_value();
	minute_button.set_text(str.str());
}