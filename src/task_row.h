/******************
task_row.h
Author: Matthew Castellana
Purpose: The GUI element which
constitutes an individual row
in the main window.
*******************/

#pragma once
#ifndef TASK_ROW_H
#define TASK_ROW_H

#include <gtkmm/label.h>
#include <gtkmm/listboxrow.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/checkbutton.h>

#include "task.h"
#include "main_screen.h"

// forward declaration
class MainScreen;

class TaskRow : public Gtk::ListBoxRow {
	public:
		TaskRow();
		TaskRow(std::string name, std::string date_string, MainScreen * new_main_window);
		virtual ~TaskRow();

	private:
		// Signal handlers
		void on_checkbox_clicked();
		void on_edit_button_clicked();

		// Container
		Gtk::EventBox event_box;
		Gtk::HBox task_container;
		Gtk::HBox event_box_container;

		// Widgets
		Gtk::CheckButton checkbox;
		Gtk::Label task_text;
		Gtk::Label date_text;
		
		// Signal Handlers
		int double_click_signal_handler_event(GdkEventButton *event);

		// Data
		MainScreen * main_window; // reference to main window

};

#endif
