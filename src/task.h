/******************
task.h
Author: Matthew Castellana
Purpose: Class which represents
a Task data item itself.
*******************/

#pragma once
#ifndef TASK_H
#define TASK_H

#include <string>
#include <chrono>
#include <iostream>

#include <glibmm/ustring.h>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/binary_object.hpp>

#include "tinyxml2.h"

using namespace tinyxml2;

class Task {
	public:
		Task();
		Task(std::string * task_name);
		Task(std::string * task_name, struct tm * new_date_time);
		Task(std::string task_name, XMLElement * task_data);


		// Setters
		void set_date_time(int new_month, int new_day, int new_year, int new_hour, int new_minute);
		void set_month(int new_month);
		void set_day_of_month(int new_day);
		void set_year(int new_year);
		void set_hour(int new_hour);
		void set_minute(int new_minute);

		// Getters
		std::string get_name() const;
		struct tm get_date_time();
		int get_month() const;
		int get_day_of_month() const;
		int get_year() const;
		int get_hour() const;
		int get_minute() const;
		std::string get_date_as_string(bool am_pm) const;
		std::chrono::system_clock::time_point get_time_point() const;

		void print(); // for debug

		bool compare(Task * other_task); // check for equality

		void create_chrono_point_from_date_time(); // used for creating chrono point from struct tm

		// Serialization - preparing custom structures for Boost Serialization
	    template<class Archive>
	    void save(Archive & ar, const unsigned int version) const
	    {
	        // Name
	        ar & name;

	        // Time - just pass over struct tm's data and reconstitute time point from it
	        int tm_sec = date_time.tm_sec;
	        int tm_min = date_time.tm_min;
	        int tm_hour = date_time.tm_hour;
	        int tm_mday = date_time.tm_mday;
	        int tm_mon = date_time.tm_mon;
	        int tm_year = date_time.tm_year;
	        int tm_isdst = date_time.tm_isdst;
	        ar & tm_sec;
	        ar & tm_min;
	        ar & tm_hour;
	        ar & tm_mday;
	        ar & tm_mon;
	        ar & tm_year;
	        ar & tm_isdst;
	    }

	    template<class Archive>
	    void load(Archive & ar, const unsigned int version)
	    {

	    	// Name
	    	ar & name;

	    	// Date
	    	int tm_sec;
	    	int tm_min;
	        int tm_hour;
	        int tm_mday;
	        int tm_mon;
	        int tm_year;
	        int tm_isdst;
	        ar & tm_sec;
	        ar & tm_min;
	        ar & tm_hour;
	        ar & tm_mday;
	        ar & tm_mon;
	        ar & tm_year;
	        ar & tm_isdst;

	        // Reconstitute struct tm
	        date_time.tm_sec = tm_sec;
	        date_time.tm_min = tm_min;
	        date_time.tm_hour = tm_hour;
	        date_time.tm_mday = tm_mday;
	        date_time.tm_mon = tm_mon;
	        date_time.tm_year = tm_year;
	        date_time.tm_isdst = tm_isdst;

	        // Reconstitute chrono point from struct tm
	        tp = std::chrono::system_clock::from_time_t(mktime(&date_time));
	    }
	    // for serialization - splits serialize function into save/load
	    BOOST_SERIALIZATION_SPLIT_MEMBER()

	private:
		std::string name;
		struct tm date_time;
		std::chrono::system_clock::time_point tp; // equal to date_time

};
#endif