/******************
sync_window.h
Author: Matthew Castellana
Purpose: Handles the dialog box for 
syncing between two different programs. 
*******************/
#pragma once
#ifndef SYNC_WINDOW_H
#define SYNC_WINDOW_H

#include <ctime>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>

#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "main_screen.h"
#include "task_list.h"
#include "tcp_connection.h"
#include "tcp_server.h"

using boost::asio::ip::tcp;

// forward declarations
class MainScreen;


class SyncWindow : public Gtk::Dialog
{
	public:
  		SyncWindow();
  		SyncWindow(MainScreen * new_main_window, int new_type); // new_type is 1 for server, 2 for client
  		virtual ~SyncWindow(); 

  	private:
  		int type; // 1 - server, 2 - client
  		Gtk::Label status; // status text
  		Gtk::Button start; // start/cancel button for server
  		Gtk::Button sync; // sync button for client
  		bool cancel; // whether to cancel syncing (as the server)

  		// Boost IO_Service
  		boost::asio::io_service io_service;

  		// Sync Thread for handling syncing without locking up application
  		std::thread * sync_thread;

  		// Used for switching "Start" callback to/from "Cancel" callback
  		sigc::connection c;

  		// Button Handlers
  		void server_start_button_clicked();
  		void server_cancel_button_clicked();
  		void client_sync_button_clicked();

  		// Sets up the thread controlling syncing for server
  		void look_for_connections_handler();

  		// Reference to main window
  		MainScreen * main_window;
};

#endif