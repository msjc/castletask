/******************
main_screen.h
Author: Matthew Castellana
Purpose: Handles the code for the main screen
which appears when the application is first called.

*******************/
#pragma once
#ifndef MAIN_SCREEN_H
#define MAIN_SCREEN_H

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/stat.h>
#include <gtkmm/button.h>
#include <gtkmm/window.h>
#include <gtkmm/entry.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/label.h>
#include <gtkmm/listbox.h>
#include <gtkmm/scrolledwindow.h>	

#include <boost/asio.hpp>

#include "controller.h"
#include "sync_window.h"
#include "task_creation_window.h"
#include "task_list.h"
#include "task_row.h"
#include "tinyxml2.h"

using namespace tinyxml2;

// forward declarations
class SyncWindow;
class TaskRow;
class TaskCreationWindow;
class Controller;



class MainScreen : public Gtk::Window
{

public:
  MainScreen();
  virtual ~MainScreen();

  void set_controller(Controller * new_controller);

  void clear_task_list();

  void push_back_task_row(std::string name, std::string date_string);

  void add_task(Task * new_task);
  void edit_task(Task * task_to_edit, Task * edited_task);
  void remove_task(std::string name, std::string date_string);

  // Signal Handlers
  void edit_task_button_clicked(std::string name, std::string date_string);


  // Controller pass-throughs
  TaskList* get_list_for_sync();
  void sync_task_lists(TaskList* new_task_list);

private:
  // Signal handlers:
  void on_button_clicked();
  void add_task_button_clicked();
  void sync_as_server_button_clicked();
  void sync_as_client_button_clicked();

  void closeWindow();

  // Boost IO_Service
  boost::asio::io_service io_service;

  // Container widgets:
  Gtk::VBox main_layout; // top-level layout
  Gtk::HBox top_row; // fixed top row of buttons
  Gtk::ScrolledWindow m_ScrolledWindow; // allows scrolling up and down of tasks,
                                        // contains list_box
  Gtk::ListBox m_ListBox; // contains the tasks
  TaskCreationWindow * new_task_window; // pointer to dialog for task creation
  SyncWindow * new_sync_window; // pointer to dialog for syncing

  // Buttons
  Gtk::Button add_task_button;
  Gtk::Button sync_as_server_button;
  Gtk::Button sync_as_client_button;

  // Row Storage
  std::vector<TaskRow*> task_row_array; // stores the GUI TaskRow elements

  Controller * controller;
};

#endif
