/******************
task.cpp
Author: Matthew Castellana
*******************/

#include <string>
#include <iostream>

#include "task.h"

Task::Task(){
	
}

Task::Task(std::string * task_name){
	name = *task_name;
}

Task::Task(std::string * task_name, struct tm * new_date_time){
	name = *task_name;
	date_time = *new_date_time;
	tp = std::chrono::system_clock::from_time_t(mktime(&date_time));
}

Task::Task(std::string task_name, XMLElement * task_data){
	name = task_name;
	int month, day, year, hour, minute;
  	task_data->QueryIntAttribute("Month", &month);
  	task_data->QueryIntAttribute("DayOfMonth", &day);
  	task_data->QueryIntAttribute("Year", &year);
  	task_data->QueryIntAttribute("Hour", &hour);
  	task_data->QueryIntAttribute("Minute", &minute);
  	set_date_time(month, day, year, hour, minute);
  	create_chrono_point_from_date_time(); // set the chrono point from the date_time tm struct,
                                                  // since it is also used
}

// Setters
void Task::set_date_time(int new_month, int new_day, int new_year, int new_hour, int new_minute){
	this->set_month(new_month);
	this->set_day_of_month(new_day);
	this->set_year(new_year);
	this->set_hour(new_hour);
	this->set_minute(new_minute);
	date_time.tm_sec = 0;  // we don't care about seconds
	date_time.tm_isdst = -1; // check locale for DST
}

void Task::set_month(int new_month){
	date_time.tm_mon = new_month;
}
void Task::set_day_of_month(int new_day){
	date_time.tm_mday = new_day;
}
void Task::set_year(int new_year){
	date_time.tm_year = new_year - 1900;
}
void Task::set_hour(int new_hour){
	date_time.tm_hour = new_hour;
}
void Task::set_minute(int new_minute){
	date_time.tm_min = new_minute;
}


// Getters
std::string Task::get_name() const {
	return name;
}

struct tm Task::get_date_time() {
	return date_time;
}

int Task::get_month() const{
	return date_time.tm_mon;
}
int Task::get_day_of_month() const {
	return date_time.tm_mday;
}
int Task::get_year() const{
	return date_time.tm_year + 1900; // year in regular, not tm time
}

int Task::get_hour() const {
	return date_time.tm_hour; // hour in 0-23 time
}

int Task::get_minute() const {
	return date_time.tm_min;
}

// Get the date as a formatted string.
// Used for display in the TaskRow.
std::string Task::get_date_as_string(bool am_pm) const {
	std::stringstream date_string; // the date stringstream

	// M/D/YYYY
	date_string << date_time.tm_mon + 1 << "/" << date_time.tm_mday << "/" << date_time.tm_year + 1900 << " ";

	std::cout << "Date String is " << date_string.str() << std::endl;

	// Hour
	int hour = date_time.tm_hour;

	// Figure out AM/PM
	std::string a_p;
	if ((am_pm) && (hour > 12)){ // currently 24-hour clock not supported
		hour -= 12;
		a_p = "P.M.";
	}
	else {
		a_p = "A.M.";
	}

	// Format hour
	if (hour == 0){ // display midnight as 12
		date_string << "12";
	}
	else {
		date_string << hour;
	}
	
	date_string  << ":";
	
	if (date_time.tm_min < 10){ // add leading 0 to single-digit minute value
		date_string << "0";
	} 
	date_string << date_time.tm_min << " " << a_p;

	return date_string.str();
}

std::chrono::system_clock::time_point Task::get_time_point() const{
	return this->tp;
}

// Print the task name to Terminal
void Task::print(){
	std::cout << name << std::endl;
}

// Compare another task to this one.
// Tasks are equivalent if their name and time are equivalent.
bool Task::compare(Task * other_task){
	if (name == other_task->get_name()){
		std::cout << "name equal" << std::endl;
		if (this->get_time_point() == other_task->get_time_point()){
			std::cout << "time equal" << std::endl;
			return true;
		}
		else{
			std::cout << "Time not equal" << std::endl;
			std::time_t t1 = std::chrono::system_clock::to_time_t(this->get_time_point());
			std::time_t t2 = std::chrono::system_clock::to_time_t(other_task->get_time_point());
			std::cout << std::ctime(&t1) << std::endl;
			std::cout << std::ctime(&t2) << std::endl;
			//std::cout << tp << std::endl;
			//std::cout << other_task->get_time_point() << std::endl;
			return false;
		}
	}
	else {
		return false;
	}
}

// Sets the chrono point variable (tp) from the date_time variable
void Task::create_chrono_point_from_date_time(){
	tp = std::chrono::system_clock::from_time_t(mktime(&date_time));
}