/******************
task_creation_window.h
Author: Matthew Castellana
Purpose: Code for the window
which allows the user to input
a new task.
*******************/

#pragma once
#ifndef TASK_CREATION_WINDOW_H
#define TASK_CREATION_WINDOW_H

#include <iostream>
#include <iomanip>

#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/hvbox.h>
#include <gtkmm/button.h>
#include <gtkmm/calendar.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/comboboxtext.h>

#include <pangomm/attrlist.h>
#include <pangomm/attributes.h>

#include "main_screen.h"
#include "task.h"
#include "time.h"

// forward declarations
class MainScreen;


class TaskCreationWindow : public Gtk::Dialog
{
	public:
  		TaskCreationWindow();
  		TaskCreationWindow(MainScreen * new_main_window);
  		TaskCreationWindow(MainScreen * new_main_window, Task * new_task_to_edit); // for editing tasks

  		virtual ~TaskCreationWindow();

  		void initialize_focus(); // places the cursor in the 
  								 // "Name" text field

	private:
		// Signal handlers
		void ok_button_clicked();

		// Setup
		void initialize_window(MainScreen * new_main_window);

		// Time
		void configure_time_divider(); // change the size of the ":" between hour and minute
		void set_time(); // initialize time to current time
		void minute_digits_changed(); // adds leading zero to minutes for text parsing

		// Edited Status
		bool edited_task;

		// Containers
		Gtk::HBox two_columns; // top layout
		Gtk::VBox date_time_box; // holds clock and calendar
		Gtk::HBox time_box; // holds clock
		Gtk::HBox task_name_entry_row; // holds task label and text entry

		// Entries
		Gtk::Entry task_name_entry; // task name entry

		// Labels
		Gtk::Label enter_text_label; // task name label

		// Buttons
		Gtk::Button ok_button;

		// Date
		Gtk::Calendar calendar;

		// Time
		Gtk::SpinButton hour_button;
		Gtk::Label time_divider;
		Gtk::SpinButton minute_button;
		Gtk::ComboBoxText am_pm;

		// Task Reference
		Task * task_to_edit;

		// Main Window Reference
		MainScreen * main_window;

};
#endif