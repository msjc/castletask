/******************
task_list.cpp
Author: Matthew Castellana
*******************/

#include <string>
#include <iostream>
#include "task_list.h"

TaskList::TaskList(){

}

TaskList::TaskList(std::string task_list_name){
	name = task_list_name;
}

TaskList::~TaskList(){
}

// Getters
int TaskList::getLength(){
	return list.size();
}

std::string TaskList::getName(){
	return name;
}

// Returns task located at position.
//@todo: error checking/input validation
Task * TaskList::getTaskAtPosition(int position){
	if (position < list.size()){
		return list[position];
	}
	else{
		return NULL;
	}
}

Task * TaskList::fetch_task(std::string name, std::string date_string){

	int task_index = fetch_task_index(name, date_string);
	if (task_index < list.size()){ // did not find
		return list[task_index];
	}
	else {
		throw -1;
	}
}

int TaskList::fetch_task_index(std::string name, std::string date_string){

	// ADD ERROR CODE HERE OR AT OTHER FUNCTION
	bool ampm = true; // 24 hour clock not currently supported

	auto it = find_if(list.begin(), list.end(), [&name, &date_string, &ampm](Task * task_obj){return ((task_obj->get_name().compare(name) == 0) && (task_obj->get_date_as_string(ampm).compare(date_string) == 0));});

	std::cout << "TaskList:fetch_task_index: task index is " << it-list.begin() << std::endl;

	return it - list.begin(); // returns the index of the entry in the vector - the size if not found
}

void TaskList::create_task(std::string task_name, XMLElement * task_data){
	// create the task
	Task new_task(task_name, task_data);

	// add it to the list
	add_task(&new_task);
}

void TaskList::add_task(Task * new_task){
	// Add the task to the end
	list.push_back(new_task);

	// Sort oldest to newest
	std::sort(list.begin(), list.end(), [](Task * first, Task * second){
		return first->get_time_point() < second->get_time_point();
	});
}

void TaskList::edit_task(Task * task_to_edit, Task * edited_task){
	// ADD ERROR CODE HERE OR AT OTHER FUNCTION
	bool ampm = true; // 24 hour clock not currently supported

	std::cout << "Begin edit task" << std::endl;
	std::cout << task_to_edit->get_date_as_string(true) << std::endl;

	// find the index of the task in the task list
	int task_index = fetch_task_index(task_to_edit->get_name(), task_to_edit->get_date_as_string(ampm));

	if (task_index >= list.size()){ // did not find
		std::cerr << "TaskList Error: Task to edit not found in task list." << std::endl;
		exit(2);
	}

	std::cout << "Inserting task" << std::endl;
	// insert the new task
	list[task_index] = edited_task;

	std::cout << "Deleting task" << std::endl;
	// delete the old task
	delete task_to_edit;

	std::cout << "Sorting task" << std::endl;
	// Sort oldest to newest
	std::sort(list.begin(), list.end(), [](Task * first, Task * second){
		return first->get_time_point() < second->get_time_point();
	});

}

void TaskList::remove_task(std::string name, std::string date_string){
	for (int i = 0; i < list.size(); ++i){
		if (name == list[i]->get_name()){
			if (date_string == list[i]->get_date_as_string(true)){
				list.erase(list.begin() + i);
				return;
			}
		}
	}	
}

// Print each task in the list
void TaskList::print(){
	for (int i = 0; i < list.size(); ++i){
		list[i]->print();
	}
}