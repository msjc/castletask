CastleTask
==========

## Introduction
CastleTask is a simple task list program which allows multiple instances of the program running on different devices to sync with one another. Tasks are loaded and saved in XML format in the user's Documents folder under CastleTask/tasks.xml. Tasks can be "backed up" by simply copying this file.

The current implementation of the software allows for very simple task management: a task can only consist of a name and a date/time. Tasks are sorted by date/time, with the earliest appearing at the top. Clicking the checkbox removes the task from the list.

CastleTask is currently only supported on Desktop Linux.

## Syncing Tasks
To sync tasks between instances, both devices running CastleTask must be connected to the same local network. Then, follow these steps:

1. Click "Sync as Server" on the first instance. A dialog will pop up. Click "Start".
2. Click "Sync as Client" on the second instance. A dialog will pop up. Click "Sync". The tasks from the "Server" instance should appear on the "Client" instance.
3. Click "Cancel" on the Server dialog. Close out of the dialogs in both instances.
4. Repeat the process with the second instance as the Server, and the first instance as the Client, to complete the sync.
    
A future update will simplify the process. Currently, data is not encrypted during transfer.

## Running the program
1. Download the /bin folder and place the CastleTask binary file (and logo) wherever you wish.
2. Edit the .desktop file such that the Exec path leads to the binary file, and the Icon path leads to the logo file. Note that $USER is just a non-working placeholder, and you will need to fill in the full direct path.
3. To get the logo to appear in Ubuntu's Dock, copy the .desktop file to /usr/share/applications by typing "sudo cp /path/to/file /usr/share/applications" in Terminal and entering your password when prompted.

Future iterations of the program will focus on improving the installation process.

## Compiling from source
To compile from source:

1. Download the /src directory.
2. Install dependencies - CMake, Boost, GTKmm3.
3. Compile using the provided CMakeLists.txt file.

The program also uses TinyXML2, but the necessary files are included in the /src directory.

## Future Goals

* improve GUI layout/design
* add more advanced task functionality/classification
* improve the synchronization process
* encrypt the synchronization process
* adapt code to Android/iOS for mobile task management